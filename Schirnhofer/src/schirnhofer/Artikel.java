/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schirnhofer;

import java.util.Date;

/**
 *
 * @author Chrisi
 */
    
    

public class Artikel {
    
    private int artnr;
    private String artbez;
    private String artname;
    private Date mhd;
    private int menge;
    private int menge_change;

    public Artikel(int artnr, String artbez, String artname, Date mhd, int menge, int menge_change) {
        this.artnr = artnr;
        this.artbez = artbez;
        this.artname = artname;
        this.mhd = mhd;
        this.menge = menge;
        this.menge_change = menge_change;
    }
    
     public Artikel() {
        this.artnr = 0;
        this.artbez = new String();
        this.artname = new String();
        this.mhd = new Date();
        this.menge = 0;
        this.menge_change = 0;
    }

    /**
     * @return the artnr
     */
    public int getArtnr() {
        return artnr;
    }

    /**
     * @param artnr the artnr to set
     */
    public void setArtnr(int artnr) {
        this.artnr = artnr;
    }

    /**
     * @return the artbez
     */
    public String getArtbez() {
        return artbez;
    }

    /**
     * @param artbez the artbez to set
     */
    public void setArtbez(String artbez) {
        this.artbez = artbez;
    }

    /**
     * @return the artname
     */
    public String getArtname() {
        return artname;
    }

    /**
     * @param artname the artname to set
     */
    public void setArtname(String artname) {
        this.artname = artname;
    }

    /**
     * @return the mhd
     */
    public Date getMhd() {
        return mhd;
    }

    /**
     * @param mhd the mhd to set
     */
    public void setMhd(Date mhd) {
        this.mhd = mhd;
    }

    /**
     * @return the menge
     */
    public int getMenge() {
        return menge;
    }

    /**
     * @param menge the menge to set
     */
    public void setMenge(int menge) {
        this.menge = menge;
    }

    /**
     * @return the menge_change
     */
    public int getMenge_change() {
        return menge_change;
    }

    /**
     * @param menge_change the menge_change to set
     */
    public void setMenge_change(int menge_change) {
        this.menge_change = menge_change;
    }
    
    
}
