/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package schirnhofer;

/**
 *
 * @author Chrisi
 */
public class User {
    
    private String username;
    private String passwort;
    private int priv;

    
    public User(String username, String passwort, int priv) {
        this.username = username;
        this.passwort = passwort;
        this.priv = priv;
    }

     public User() {
        this.username = new String();
        this.passwort = new String();
        this.priv = 0;
    }
    
    
    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the passwort
     */
    public String getPasswort() {
        return passwort;
    }

    /**
     * @param passwort the passwort to set
     */
    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    /**
     * @return the priv
     */
    public int getPriv() {
        return priv;
    }

    /**
     * @param priv the priv to set
     */
    public void setPriv(int priv) {
        this.priv = priv;
    }
    
}
